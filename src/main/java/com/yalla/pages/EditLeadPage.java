package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class EditLeadPage extends Annotations {

	public EditLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="updateLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.ID, using="updateLeadForm_lastName") WebElement eleUpdate;
	public EditLeadPage updateLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}
	public ViewLeadPage clickUpdate() {
		click(eleUpdate);
		return new ViewLeadPage();
	}
}
