package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Then;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="viewLead_statusId_sp") WebElement eleCreatLead;
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement eleEdit;
	@FindBy(how=How.ID, using="viewLead_lastName_sp") WebElement eleUpdateLead;
	@Then("Verify Create Lead is success")
		public ViewLeadPage verifyLeadCreated() {
		verifyDisplayed(eleCreatLead);
		return this;
	}
	public EditLeadPage clickEdit() {
		click(eleEdit);
		return new EditLeadPage();
	}
	public ViewLeadPage verifyLeadUpdated() {
		verifyDisplayed(eleUpdateLead);
		return this;
	}

}
