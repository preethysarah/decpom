package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Given;

public class MyLeadsPage extends Annotations {

	public MyLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH, using="//a[text()='Create Lead']") WebElement eleCreateLead;
	@FindBy(how=How.XPATH, using="//a[text()='Find Lead']") WebElement eleFindLead;
	
	@Given("Click on Create Lead link")
	public CreateLeadPage clickCreateLead()
	{
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	public FindLeadsPage clickFindLead() throws InterruptedException
	{
		Thread.sleep(6000);
		click(eleFindLead);
		return new FindLeadsPage();
	}
}
