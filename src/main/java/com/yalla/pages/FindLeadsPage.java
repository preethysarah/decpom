package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations {
	
	public FindLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleFirstName;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeads;
	@FindBy(how=How.LINK_TEXT, using="10106") WebElement eleFind;
	public FindLeadsPage enterFirstName(String data)
	{
		clearAndType(eleFirstName, data);
		return this;
	}
	public FindLeadsPage clickFindLeads() {
		click(eleFindLeads);
		return this;
	}
	public ViewLeadPage clickFindFirst() {
	click(eleFind);	
	return new ViewLeadPage();
	}

}
