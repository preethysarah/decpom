package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{

	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLName;
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCName;
	@FindBy(how=How.XPATH, using="//input[@value='Create Lead']") WebElement buttonCL;
	
	@Given("Enter Company name as (.*)")
	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(eleCName, data);
		return this;
	}
	@Given("Enter First name as (.*)")
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFName,data);
		return this;
	}
	@Given("Enter Last name as (.*)")
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLName,data);
		return this;
	}
	@When("Click on create lead button")
	public ViewLeadPage clickCreatLead() {
		click(buttonCL);
		return new ViewLeadPage();
	}
	

}
