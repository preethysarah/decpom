package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.EditLeadPage;
import com.yalla.pages.FindLeadsPage;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHomePage;
import com.yalla.pages.MyLeadsPage;
import com.yalla.pages.ViewLeadPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Editing Lead in leaftaps";
		author = "Gayatri";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void editLead(String uName, String pwd,String cmpnyName, String fName, String lName, String FindFName, String UpdateLName) throws InterruptedException { 
		new LoginPage().enterUserName(uName).enterPassWord(pwd) .clickLogin();
		new HomePage().clickCRMSFA();
		new MyHomePage().clickLeads();
		new MyLeadsPage().clickFindLead();
		new FindLeadsPage().enterFirstName(FindFName).clickFindLeads().clickFindFirst();
		new EditLeadPage().updateLastName(UpdateLName).clickUpdate();
		new ViewLeadPage().verifyLeadUpdated();
}
}
