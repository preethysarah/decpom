package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLeadPage;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHomePage;
import com.yalla.pages.MyLeadsPage;
import com.yalla.pages.ViewLeadPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Creating Lead in leaftaps";
		author = "Gayatri";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String pwd,String cmpnyName, String fName, String lName) throws InterruptedException { 
		new LoginPage().enterUserName(uName).enterPassWord(pwd) .clickLogin();
		new HomePage().clickCRMSFA();
		new MyHomePage().clickLeads();
		new MyLeadsPage().clickCreateLead();
		new CreateLeadPage().enterCompanyName(cmpnyName).enterFirstName(fName).enterLastName(lName).clickCreatLead();
		new ViewLeadPage().verifyLeadCreated();
	}
	
}
